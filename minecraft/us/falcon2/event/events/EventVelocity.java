package us.falcon2.event.events;

import net.minecraft.entity.Entity;
import us.falcon2.event.EventCancelable;

public class EventVelocity extends EventCancelable {
	
	private Entity entity;
	
	public void setEntity(Entity entity) {
		this.entity = entity;
	}
	
	public Entity getEntity() {
		return entity;
	}

}
