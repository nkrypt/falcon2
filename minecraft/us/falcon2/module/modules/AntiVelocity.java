package us.falcon2.module.modules;

import us.falcon2.event.Event;
import us.falcon2.event.events.EventVelocity;
import us.falcon2.module.Module;

public class AntiVelocity extends Module {
	
	@Override
	public String getName() {
		return "AntiVelocity";
	}
	
	@Override
	public void onEvent(Event event) {
		if (!getEnabled())
			return;
		if (event instanceof EventVelocity) {
			EventVelocity eventVelocity = (EventVelocity) event;
			if (eventVelocity.getEntity() == getUtils().getPlayer())
				eventVelocity.setCanceled(true);
		}
	}
	
}
