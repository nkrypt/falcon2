package us.falcon2.gui.override;

import java.util.ArrayList;
import java.util.Iterator;

import org.lwjgl.opengl.GL11;

import com.google.common.collect.Lists;

import us.falcon2.Falcon2;
import us.falcon2.module.modules.TTF;
import me.isuzutsuki.betterfonts.betterfonts.StringCache;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.ChatLine;
import net.minecraft.client.gui.GuiNewChat;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.ChatComponentText;
import net.minecraft.util.IChatComponent;
import net.minecraft.util.MathHelper;

public class GuiNewChatOverride extends GuiNewChat {

	public GuiNewChatOverride(Minecraft par1Minecraft) {
		super(par1Minecraft);
	}

	@Override
	public void func_146230_a(int p_146230_1_) {
		if (!Falcon2.getModuleManager().getModuleByClass(TTF.class).getEnabled()) {
			super.func_146230_a(p_146230_1_);
			return;
		}
		StringCache font = Falcon2.getFontManager().getFont("chatTTF");
		if (this.field_146247_f.gameSettings.chatVisibility != EntityPlayer.EnumChatVisibility.HIDDEN) {
			int var2 = this.func_146232_i();
			boolean var3 = false;
			int var4 = 0;
			int var5 = this.field_146253_i.size();
			float var6 = this.field_146247_f.gameSettings.chatOpacity * 0.9F + 0.1F;
			if (var5 > 0) {
				if (this.func_146241_e())
					var3 = true;
				float var7 = this.func_146244_h();
				int var8 = MathHelper.ceiling_float_int((float)this.func_146228_f() / var7);
				GL11.glPushMatrix();
				GL11.glTranslatef(2.0F, 20.0F, 0.0F);
				GL11.glScalef(var7, var7, 1.0F);
				int var9;
				int var11;
				int var14;
				float longestString = 0;
				int count = 0;
				for (var9 = 0; var9 + this.field_146250_j < this.field_146253_i.size() && var9 < var2; ++var9) {
					ChatLine var10 = (ChatLine)this.field_146253_i.get(var9 + this.field_146250_j);
					if (var10 != null) {
						var11 = p_146230_1_ - var10.getUpdatedCounter();
						if (var11 < 200 || var3) {
							double var12 = (double)var11 / 200.0D;
							var12 = 1.0D - var12;
							var12 *= 10.0D;
							if (var12 < 0.0D)
								var12 = 0.0D;
							if (var12 > 1.0D) 
								var12 = 1.0D;
							var12 *= var12;
							var14 = (int)(255.0D * var12);
							if (var3)
								var14 = 255;
							var14 = (int)((float)var14 * var6);
							++var4;
							if (var14 > 3) {
								float currentString = font.getStringWidth(var10.func_151461_a().getFormattedText());
								if (longestString < currentString)
									longestString = currentString;
								count++;
							}
						}
					}
				}
				if (count > 0)
					drawRectBorder(0, 0, longestString + 13.5F, -((count * 9) + 10), 0x60000000, 0xaf000000, 0.5F);
				for (var9 = 0; var9 + this.field_146250_j < this.field_146253_i.size() && var9 < var2; ++var9) {
					ChatLine var10 = (ChatLine)this.field_146253_i.get(var9 + this.field_146250_j);
					if (var10 != null) {
						var11 = p_146230_1_ - var10.getUpdatedCounter();
						if (var11 < 200 || var3) {
							double var12 = (double)var11 / 200.0D;
							var12 = 1.0D - var12;
							var12 *= 10.0D;
							if (var12 < 0.0D)
								var12 = 0.0D;
							if (var12 > 1.0D) 
								var12 = 1.0D;
							var12 *= var12;
							var14 = (int)(255.0D * var12);
							if (var3)
								var14 = 255;
							var14 = (int)((float)var14 * var6);
							++var4;
							if (var14 > 3) {
								byte var15 = 0;
								int var16 = -var9 * 9;
								String var17 = var10.func_151461_a().getFormattedText();
								font.drawString(var17, var15 + 6.5F, var16 - 13.5F, 0xffffffff, true);
								GL11.glDisable(GL11.GL_ALPHA_TEST);
							}
						}
					}
				}
				if (var3) {
					var9 = this.field_146247_f.fontRenderer.FONT_HEIGHT;
					GL11.glTranslatef(-3.0F, 0.0F, 0.0F);
					int var18 = var5 * var9 + var5;
					var11 = var4 * var9 + var4;
					int var20 = this.field_146250_j * var11 / var5;
					int var13 = var11 * var11 / var18;
					if (var18 != var11) {
						var14 = var20 > 0 ? 170 : 96;
						int var19 = this.field_146251_k ? 13382451 : 3355562;
						drawRect(0, -var20, 2, -var20 - var13, var19 + (var14 << 24));
						drawRect(2, -var20, 1, -var20 - var13, 13421772 + (var14 << 24));
					}
				}
				GL11.glPopMatrix();
			}
		}
	}

}
