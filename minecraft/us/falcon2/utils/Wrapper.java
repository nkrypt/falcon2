package us.falcon2.utils;

import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.EntityClientPlayerMP;
import net.minecraft.client.multiplayer.PlayerControllerMP;
import net.minecraft.client.multiplayer.WorldClient;
import net.minecraft.client.network.NetHandlerPlayClient;
import net.minecraft.client.settings.GameSettings;
import net.minecraft.network.play.client.C01PacketChatMessage;
import net.minecraft.util.ChatComponentText;

public class Wrapper {

	public void addChatMessage(String s) {
		getPlayer().addChatMessage(new ChatComponentText(s));
	}
	
	public PlayerControllerMP getController() {
		return getMinecraft().playerController;
	}
	
	public GameSettings getGameSettings() {
		return getMinecraft().gameSettings;
	}
	
	public Minecraft getMinecraft() {
		return Minecraft.getMinecraft();
	}
	
	public EntityClientPlayerMP getPlayer() {
		return getMinecraft().thePlayer;
	}
	
	public NetHandlerPlayClient getSendQueue() {
		return getPlayer().sendQueue;
	}
	
	public long getSysTime() {
		return System.nanoTime() / 1000000;
	}
	
	public WorldClient getWorld() {
		return getMinecraft().theWorld;
	}
	
	public void sendChatMessage(String s) {
		getSendQueue().addToSendQueue(new C01PacketChatMessage(s));
	}
	
}
