package us.falcon2.module;

import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.EntityClientPlayerMP;
import net.minecraft.client.multiplayer.WorldClient;
import us.falcon2.Falcon2;
import us.falcon2.event.Event;
import us.falcon2.managers.CommandManager;
import us.falcon2.utils.Utils;
import us.falcon2.utils.Wrapper;

public abstract class Module {
	
	protected boolean canBind;
	protected boolean canToggle;
	protected boolean enabled;
	protected boolean isHidden;
	protected String keybind;
	protected String name;
	protected boolean onArray;
	
	public Module() {
		setCanBind(true);
		setCanToggle(true);
		setIsHidden(false);
		setOnArray(true);
		onCreation();
	}
	
	public boolean getCanBind() {
		return canBind;
	}
	
	public boolean getCanToggle() {
		return canToggle;
	}
	
	public final static CommandManager getCommandManager() {
		return Falcon2.getCommandManager();
	}
	
	public boolean getEnabled() {
		return enabled;
	}
	
	public boolean getIsHidden() {
		return isHidden;
	}
	
	public String getKeybind() {
		String keybind = this.keybind;
		try {
			keybind = keybind.toUpperCase();
		} catch (Exception e) {}
		return keybind;
	}
	
	public abstract String getName();
	
	public boolean getOnArray() {
		return onArray;
	}
	
	public Utils getUtils() {
		return Falcon2.getUtils();
	}

	public void onCreation() {
		
	}
	
	public void onDisabled() {
		setEnabled(false);
	}
	
	public void onEnabled() {
		setEnabled(true);
	}
	
	public void onEvent(Event event) {
		
	}
	
	public void onToggle() {
		if (!getCanToggle())
			return;
		if (getEnabled())
			onDisabled();
		else
			onEnabled();
	}
	
	public void setCanBind(boolean canBind) {
		this.canBind = canBind;
	}
	
	public void setCanToggle(boolean canToggle) {
		this.canToggle = canToggle;
	}
	
	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}
	
	public void setIsHidden(boolean isHidden) {
		this.isHidden = isHidden;
	}
	
	public void setKeybind(String keybind) {
		this.keybind = keybind;
	}
	
	public void setOnArray(boolean onArray) {
		this.onArray = onArray;
	}
	
}
