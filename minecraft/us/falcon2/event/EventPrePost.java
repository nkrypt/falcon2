package us.falcon2.event;

public class EventPrePost extends Event {

	private EventState state;
	
	public EventState getState() {
		return state;
	}
	
	public void setState(EventState state) {
		this.state = state;
	}
	
}
