package us.falcon2.module.modules;

import java.util.ArrayList;

import net.minecraft.client.gui.FontRenderer;
import us.falcon2.Falcon2;
import us.falcon2.event.Event;
import us.falcon2.event.events.EventOnTick;
import us.falcon2.module.Module;

public class Array extends Module {

	private static ArrayList<Module> modulesOnArray = new ArrayList<Module>();
	
	public void drawArray(int screenWidth, int y) {
		if (!getEnabled())
			return;
		FontRenderer font = getUtils().getMinecraft().fontRenderer;
		for (Module module : modulesOnArray) {
			if (module.getCanToggle() && !module.getIsHidden() && module.getOnArray() && module.getEnabled()) {
				font.drawStringWithShadow(module.getName(), screenWidth - (font.getStringWidth(module.getName()) + 2), y, 0xffffffff);
				y += 9;
			}
		}
	}
	
	@Override
	public String getName() {
		return "Array";
	}
	
	private void handleArray() {
		for (Module module : Falcon2.getModuleManager().getModules()) {
			if (modulesOnArray.contains(module)) {
				if (!module.getEnabled() && modulesOnArray.contains(module))
					modulesOnArray.remove(module);
			} else if (module.getEnabled() && !module.getIsHidden() && module.getCanToggle() && module.getOnArray()) {
					modulesOnArray.add(module);
			}
		}
	}
	
	@Override
	public void onCreation() {
		setCanBind(false);
		setCanToggle(true);
		setOnArray(false);
		onToggle();
	}
	
	@Override
	public void onEvent(Event event) {
		if (event instanceof EventOnTick)
			handleArray();
	}
	
}
