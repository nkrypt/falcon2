package us.falcon2.module.modules;

import us.falcon2.event.Event;
import us.falcon2.event.EventState;
import us.falcon2.event.events.*;
import us.falcon2.module.Module;

public class Step extends Module {
	
	private static boolean shouldStep = false;

	@Override
	public String getName() {
		return "Step";
	}
	
	@Override
	public void onCreation() {
		setKeybind("M");
	}
	
	@Override
	public void onEvent(Event event) {
		if (!getEnabled())
			return;
		if (event instanceof EventOnUpdate) {
			EventOnUpdate eventOnUpdate = (EventOnUpdate) event;
			if (eventOnUpdate.getState() == EventState.POST) {
				if (!(getUtils().getPlayer().isCollidedHorizontally && getUtils().getPlayer().onGround))
					return;
				getUtils().getPlayer().boundingBox.offset(0, 0.5D, 0);
			}
		}
	}
	
}