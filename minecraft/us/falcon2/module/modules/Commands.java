package us.falcon2.module.modules;

import us.falcon2.Falcon2;
import us.falcon2.command.Command;
import us.falcon2.module.Module;

public class Commands extends Module {
	
	@Override
	public String getName() {
		return "Commands";
	}

	@Override
	public void onCreation() {
		setCanBind(false);
		setCanToggle(false);
		setIsHidden(true);
		getCommandManager().getCommands().add(new Command() {
			
			@Override
			public String getCommand() {
				return ".help";
			}
			
			@Override
			public void runCommand(String args) {
				String prefix = getUtils().getClientPrefix() + "Command: ";
				for (Command command : Falcon2.getCommandManager().getCommands())
					command.showCommands();
			}
			
			@Override
			public void showCommands() {
				getUtils().addChatMessage(getUtils().getClientPrefix() + ".help - Displays all commands.");
			}
			
		});
		getCommandManager().getCommands().add(new Command() {
			
			@Override
			public String getCommand() {
				return ".mods";
			}
			
			@Override
			public void runCommand(String args) {
				String prefix = getUtils().removeColon(getUtils().getClientPrefix());
				prefix += "[\2477Module\247f]: ";
				for (Module module : Falcon2.getModuleManager().getModules()) {
					if (!module.getIsHidden()) {
						String s = prefix;
						s += "(\2477Name\247f) " + module.getName();
						if (module.getKeybind() != null && module.getCanBind())
							s += ", (\2477Key\247f) " + module.getKeybind();
						getUtils().addChatMessage(s);
					}
				}
			}
			
			@Override
			public void showCommands() {
				getUtils().addChatMessage(getUtils().getClientPrefix() + ".mods - Lists all of the modules on the client.");
			}
			
		});
		getCommandManager().getCommands().add(new Command() {
			
			@Override
			public String getCommand() {
				return ".say";
			}
			
			@Override
			public void runCommand(String args) {
				args = args.trim();
				if (args.length() > 0 && args.length() < 101)
					getUtils().sendChatMessage(args);
			}
			
			@Override
			public void showCommands() {
				getUtils().addChatMessage(getUtils().getClientPrefix() + ".say <\2477message\247f> - Will send the specified message in chat.");
			}
			
		});
		getCommandManager().getCommands().add(new Command() {
			
			@Override
			public String getCommand() {
				return ".t";
			}
			
			@Override
			public void runCommand(String args) {
				Module module = Falcon2.getModuleManager().getModuleByName(args);
				if (module != null) {
					if (module.getCanToggle()) {
						module.onToggle();
						getUtils().addChatMessage(getUtils().getClientPrefix() + "'\2477" + module.getName() + "\247f' " + (module.getEnabled() ? "en":"dis") + "abled.");
					} else {
						if (module.getIsHidden()) {
							getUtils().addChatMessage(getUtils().getClientPrefixError() + "The module '\2477" + args + "\247f' could not be found.");
							return;
						}
						getUtils().addChatMessage(getUtils().getClientPrefixError() + "The module '\2477" + args + "\247f' can not be toggled.");
					}
				} else {
					getUtils().addChatMessage(getUtils().getClientPrefixError() + "The module '\2477" + args + "\247f' could not be found.");
				}
			}
			
			@Override
			public void showCommands() {
				getUtils().addChatMessage(getUtils().getClientPrefix() + ".t <\2477name\247f> - Toggles the specified module.");
			}
			
		});
	}
	
}
