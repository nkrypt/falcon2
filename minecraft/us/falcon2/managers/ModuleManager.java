package us.falcon2.managers;

import java.util.Arrays;

import org.lwjgl.input.Keyboard;

import us.falcon2.module.Module;
import us.falcon2.module.modules.*;

public class ModuleManager {

	private static Module[] modules = null;

	public ModuleManager() {
		modules = new Module[] {
				new AntiVelocity(),
				new Array(),
				new Aura(),
				new AutoEat(),
				new ChestRaider(),
				new Commands(),
				new Criticals(),
				new Fullbright(),
				new RapeAura(),
				new Sprint(),
				new Step(),
				new Timer(),
				new TTF()
		};
	}

	public final static Module[] getModules() {
		return modules;
	}
	
	public Module getModuleByClass(Class<?> clazz) {
		for (Module module : getModules()) {
			if (module.getClass() == clazz)
				return module;
		}
		return null;
	}
	
	public Module getModuleByName(String name) {
		for (Module module : getModules()) {
			if (module.getName().equalsIgnoreCase(name))
				return module;
		}
		return null;
	}
	
	public void runKeybind(int key) {
		for (Module module : getModules()) {
			if (module.getKeybind() != null) {
				int stringToInt = Keyboard.getKeyIndex(module.getKeybind());
				if (key == stringToInt && module.getCanToggle())
					module.onToggle();
			}
		}
	}
	
}