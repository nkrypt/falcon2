package us.falcon2.module.modules;

import us.falcon2.Falcon2;
import us.falcon2.event.*;
import us.falcon2.event.events.*;
import us.falcon2.module.Module;

public class Fullbright extends Module {

	@Override
	public String getName() {
		return "Fullbright";
	}
	
	@Override
	public void onCreation() {
		setKeybind("B");
	}
	
	@Override
	public void onEvent(Event event) {
		if (!getEnabled())
			return;
		if (event instanceof EventBrightness) {
			EventBrightness eventBrightness = (EventBrightness) event;
			eventBrightness.brightness = 10F;
		}
	}
	
}