package us.falcon2.module.modules;

import us.falcon2.command.Command;
import us.falcon2.event.Event;
import us.falcon2.module.Module;

public class TTF extends Module {
	
	@Override
	public String getName() {
		return "TTF";
	}
	
	@Override
	public void onCreation() {
		setCanBind(false);
		setOnArray(false);
	}
	
}
