package us.falcon2;

import us.falcon2.managers.*;
import us.falcon2.utils.*;

public class Falcon2 {
	
	private final static CommandManager commandManager = new CommandManager();
	private final static EventManager eventManager = new EventManager();
	private final static FontManager fontManager = new FontManager();
	private final static KeybindManager keybindManager = new KeybindManager();
	private final static ModuleManager moduleManager = new ModuleManager();
	private final static Utils utils = new Utils();
	private final static Wrapper wrapper = new Wrapper();
	
	public final static CommandManager getCommandManager() {
		return commandManager;
	}
	
	public final static EventManager getEventManager() {
		return eventManager;
	}
	
	public final static FontManager getFontManager() {
		return fontManager;
	}
	
	public final static KeybindManager getKeybindManager() {
		return keybindManager;
	}
	
	public final static ModuleManager getModuleManager() {
		return moduleManager;
	}

	public final static Utils getUtils() {
		return utils;
	}
	
	public final static Wrapper getWrapper() {
		return wrapper;
	}
	
}
