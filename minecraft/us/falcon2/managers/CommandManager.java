package us.falcon2.managers;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

import us.falcon2.Falcon2;
import us.falcon2.command.Command;

public class CommandManager {

	private ArrayList<Command> commands = new ArrayList<Command>();
	
	public void callCommand(String s) {
		if (s.startsWith(".")) {
			for (Command command : getCommands()) {
				String s2 = s;
				if (s.contains(" "))
					s2 = s.split(" ")[0];
				if (command.getCommand().equalsIgnoreCase(s2)) {
					try {
						command.runCommand(s.substring(s2.length()).trim());
					} catch (Exception e) {
						Falcon2.getUtils().addChatMessage(Falcon2.getUtils().getClientPrefixError() + "Failed to execute command.");
					}
				}
			}
		} else if (s.length() > 0) {
			Falcon2.getWrapper().sendChatMessage(s);
		}
	}
	
	public ArrayList<Command> getCommands() {
		Collections.sort(commands, new CommandComparator());
		return commands;
	}
	
	class CommandComparator implements Comparator<Command> {
		
		@Override
		public int compare(Command c1, Command c2) {
			String c1s = c1.getCommand();
			String c2s = c2.getCommand();
			return c1s.compareTo(c2s);
		}
		
	}
	
}
