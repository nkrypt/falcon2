package us.falcon2.command;

public abstract class Command {

	public abstract String getCommand();
	
	public abstract void runCommand(String args);
	
	public void showCommands() {
		
	}

}
