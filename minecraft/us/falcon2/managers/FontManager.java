package us.falcon2.managers;

import java.awt.Font;
import java.io.InputStream;
import java.util.HashMap;
import me.isuzutsuki.betterfonts.betterfonts.StringCache;

public class FontManager {
	
	private final static HashMap<String, StringCache> fonts = new HashMap<String, StringCache>();
	
	public FontManager() {
		addFont("onScreenFont", "resource:/Aller_Rg.ttf", 18F, true);
		addFont("chatTTF", "resource:/Aller_Rg.ttf", 17F, true);
		addFont("guiButtonFont", "resource:/Aller_Rg.ttf", 20F, true);
		addFont("titleFont", "resource:/Aller_Rg.ttf", 20F, true);
	}
	
	public void addFont(String id, String fontName, float fontSize, boolean antiAlias) {
		StringCache font = new StringCache(new int[32]);
		if (fontName.startsWith("resource:"))
			font.setDefaultFont(getFontFile(fontName.substring("resource:".length())), fontSize, antiAlias);
		else
			font.setDefaultFont(fontName, (int) fontSize, antiAlias);
		fonts.put(id, font);
	}
	
	public StringCache getFont(String id) {
		return fonts.get(id);
	}
	
	public Font getFontFile(String font) {
		try {
			return Font.createFont(Font.TRUETYPE_FONT, FontManager.class.getResourceAsStream(font));
		} catch (Exception e) {}
		return null;
	}
	
}