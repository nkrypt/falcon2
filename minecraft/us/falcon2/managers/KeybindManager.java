package us.falcon2.managers;

import org.lwjgl.input.Keyboard;

import us.falcon2.Falcon2;

public class KeybindManager {

	public void runKey(Integer key) {
		if (key == null)
			return;
		if (key == Keyboard.KEY_RSHIFT) {
			//Display GUI.
			return;
		}
		Falcon2.getModuleManager().runKeybind(key);
	}
	
}
