package us.falcon2.event;

public class EventCancelable extends Event {
	
	private boolean canceled;
	
	public void setCanceled(boolean canceled) {
		this.canceled = canceled;
	}
	
	public boolean getCanceled() {
		return canceled;
	}
	
}
