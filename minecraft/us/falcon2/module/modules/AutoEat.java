package us.falcon2.module.modules;

import java.util.ArrayList;

import net.minecraft.item.*;
import net.minecraft.network.play.client.C08PacketPlayerBlockPlacement;
import us.falcon2.command.Command;
import us.falcon2.event.*;
import us.falcon2.event.events.*;
import us.falcon2.module.Module;

public class AutoEat extends Module {

	private static int lastSlot = 0;
	public static int itemID = 282;
	public static int threshold = 12;
	private static ArrayList<SlotItem> slots = new ArrayList<SlotItem>();
	
	@Override
	public String getName() {
		return "AutoEat";
	}
	
	private SlotItem getAvailableInventorySlot() {
		for (SlotItem slotItem : slots) {
			if (slotItem.getStack() != null && slotItem.getItemID() == itemID && slotItem.getSlotID() <= 35)
				return slotItem;
		}
		return null;
	}
	
	public void handleEat() {
		lastSlot = getUtils().getPlayer().inventory.currentItem;
		slots = new ArrayList<SlotItem>();
		for (int slot = 9; slot <= 44; slot++) {
			ItemStack stack = null;
			try {
				stack = getUtils().getPlayer().inventoryContainer.getSlot(slot).getStack();
			} catch (Exception e) { }
			slots.add(new SlotItem(slot, stack));
		}
		if (slots.isEmpty())
			return;
		for (SlotItem slotItem : slots) {
			if (slotItem.getStack() != null && slotItem.getItemID() == itemID && slotItem.getSlotID() >= 36) {
				getUtils().getPlayer().inventory.currentItem = slotItem.getSlotID() - 36;
				getUtils().getController().updateController();
				getUtils().getSendQueue().addToSendQueue(new C08PacketPlayerBlockPlacement(0, 0, 0, -1, slotItem.getStack(), -1, -1, -1));
				break;
			}
		}
		getUtils().getPlayer().inventory.currentItem = lastSlot;
	}
	
	public void handlePlacement() {
		if (slots.isEmpty())
			return;
		if (!isHotbarSlotAvailable())
			return;
		for (SlotItem slotItem : slots) {
			if (slotItem.getStack() == null && slotItem.getSlotID() >= 36) {
				if (getAvailableInventorySlot() != null) {
					getUtils().getController().windowClick(0, getAvailableInventorySlot().getSlotID(), 0, 0, getUtils().getPlayer());
					getUtils().getController().windowClick(0, slotItem.getSlotID(), 0, 0, getUtils().getPlayer());
					return;
				}
			}
		}
	}
	
	private boolean isHotbarSlotAvailable() {
		for (SlotItem slotItem : slots) {
			if (slotItem.getStack() == null && slotItem.getSlotID() >= 36)
				return true;
		}
		return false;
	}
	
	@Override
	public void onCreation() {
		getCommandManager().getCommands().add(new Command() {

			@Override
			public String getCommand() {
				return ".ae";
			}

			@Override
			public void runCommand(String args) {
				String[] arguments = args.split(" ");
				if (arguments[0].equalsIgnoreCase("id")) {
					int value = Integer.parseInt(arguments[1]);
					itemID = value;
					getUtils().addChatMessage(getUtils().getClientPrefix() + "AutoEat ID set to '\2477" + value + "\247f'.");
				} else if (arguments[0].equalsIgnoreCase("th")){
					int value = Integer.parseInt(arguments[1]);
					if (value > 0  && value < 20) {
						threshold = value;
						getUtils().addChatMessage(getUtils().getClientPrefix() + "AutoEat ID set to '\2477" + value + "\247f'.");
					} else {
						getUtils().addChatMessage(getUtils().getClientPrefixError() + "Threshold can only be set to \24771-19\247f.");
					}
				}
			}

			@Override
			public void showCommands() {
				getUtils().addChatMessage(getUtils().getClientPrefix() + ".ae id <\2477value\247f> - Sets the AutoEat ID.");
				getUtils().addChatMessage(getUtils().getClientPrefix() + ".ae th <\2477value\247f> - Sets the AutoEat threshold.");
			}

		});
	}
	
	@Override
	public void onEvent(Event event) {
		if (!getEnabled())
			return;
		if (getUtils().getPlayer().getHealth() > threshold)
			return;
		if (event instanceof EventOnUpdate) {
			EventOnUpdate eventOnUpdate = (EventOnUpdate) event;
			if (eventOnUpdate.getState() == EventState.PRE) {
				handlePlacement();
			}
		} else if (event instanceof EventMotionUpdate) {
			EventMotionUpdate eventMotionUpdate = (EventMotionUpdate) event;
			if (eventMotionUpdate.getState() == EventState.PRE)
				handleEat();
		}
	}
	
	class SlotItem {
		
		private int itemID;
		private int slotID;
		private ItemStack stack;
		
		public SlotItem(int slotID, ItemStack stack) {
			this.slotID = slotID;
			this.stack = stack;
			try {
				itemID = Item.getIdFromItem(stack.getItem());
			} catch (Exception e) {
				itemID = -1;
			}
		}
		
		public int getItemID() {
			return itemID;
		}
		
		public ItemStack getStack() {
			return stack;
		}
		
		public int getSlotID() {
			return slotID;
		}
		
	}
	
}