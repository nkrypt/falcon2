package us.falcon2.utils;

import us.falcon2.Falcon2;
import us.falcon2.module.Module;

public class Utils extends Wrapper {

	public String getClientPrefix() {
		return "[\2477Falcon\247c2\247f]: ";
	}
	
	public String getClientPrefixError() {
		return removeColon(getClientPrefix()) + "[\2477Error\247f]: ";
	}
	
	public Module getModuleByClass(Class<?> clazz) {
		for (Module module : Falcon2.getModuleManager().getModules()) {
			if (module.getClass() == clazz)
				return module;
		}
		return null;
	}
	
	public String removeColon(String str) {
		int lengthA = str.length();
		return str.substring(0, lengthA - 2) + " ";
	}
	
}