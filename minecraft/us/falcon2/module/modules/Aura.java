package us.falcon2.module.modules;

import java.util.ArrayList;
import net.minecraft.client.entity.EntityClientPlayerMP;
import net.minecraft.entity.*;
import net.minecraft.network.play.client.C03PacketPlayer;
import net.minecraft.util.MathHelper;
import us.falcon2.Falcon2;
import us.falcon2.event.*;
import us.falcon2.event.events.*;
import us.falcon2.module.Module;

public class Aura extends Module {

	private final static AuraUtils auraUtils = new AuraUtils();
	public static Long delay = 125L;
	public static Entity target = null;
	public static Long last = null;
	public static float reach = 3.8F;
	
	@Override
	public String getName() {
		return "Aura";
	}
	
	protected void handleAura() {
		delay = 1L;
		reach = 4.25F;
		if (last == null || getUtils().getSysTime() >= last + delay) {
			last = getUtils().getSysTime();
			setTargetEntity();
			if (target != null) {
				EventAuraAttack eventAuraAttack = new EventAuraAttack();
				eventAuraAttack.setState(EventState.PRE);
				Falcon2.getEventManager().callEvent(eventAuraAttack);
				getUtils().getPlayer().swingItem();
				getUtils().getController().attackEntity(getUtils().getPlayer(), target);
				eventAuraAttack.setState(EventState.POST);
				Falcon2.getEventManager().callEvent(eventAuraAttack);
			}
		} else {
			target = null;
		}
	}
	
	@Override
	public void onCreation() {
		setKeybind("R");
	}
	
	@Override
	public void onEvent(Event event) {
		if (!getEnabled())
			return;
		if (event instanceof EventMotionUpdate) {
			EventMotionUpdate eventOnUpdate = (EventMotionUpdate) event;
			if (eventOnUpdate.getState() == EventState.PRE)
				handleAura();
		} else if (event instanceof EventRotation) {
			EventRotation eventRotation = (EventRotation) event;
			if (eventRotation.getState() == EventState.PRE) {
				if (target != null)
					auraUtils.faceEntity(target);
				setRotation(eventRotation, AuraUtils.aimYaw, AuraUtils.aimPitch);
			}
		}
	}
	
	private void setRotation(EventRotation eventRotation, Float yaw, Float pitch) {
		if (yaw == null || pitch == null)
			return;
		double x = getUtils().getPlayer().posX;
		double minY = getUtils().getPlayer().boundingBox.minY;
		double y = getUtils().getPlayer().posY;
		double z = getUtils().getPlayer().posZ;
		boolean onGround = getUtils().getPlayer().onGround;
		eventRotation.setPacket(new C03PacketPlayer.C06PacketPlayerPosLook(x, minY, y, z, yaw, pitch, onGround));
	}
	
	protected void setTargetEntity() {
		Entity target = null;
		for (Entity current : new ArrayList<Entity>(getUtils().getWorld().loadedEntityList)) {
			if (current != null && current instanceof EntityLivingBase && current != getUtils().getPlayer()) {
				float curDist = getUtils().getPlayer().getDistanceToEntity(current);
				if (!current.isDead && curDist <= reach && getUtils().getPlayer().canEntityBeSeen(current)) {
					if (target != null) {
						float targetDist = getUtils().getPlayer().getDistanceToEntity(target);
						if (curDist < targetDist)
							target = current;
					} else {
						target = current;
					}
				}
			}
		}
		this.target = target;
	}
	
	private static class AuraUtils extends Aura {
		
		public static Float aimPitch = null;
		public static Float aimYaw = null;
		public int facePitchRadius = 10;
		public int faceYawRadius = 30;
		public int lookSpeed = 120;
		
		public void faceEntity(Entity par1Entity) {
			if (aimPitch == null)
				aimPitch = getPlayer().rotationPitch;
			if (aimYaw == null)
				aimYaw = getPlayer().rotationYaw;
			double var4 = par1Entity.posX - getPlayer().posX;
			double var6 = par1Entity.posZ - getPlayer().posZ;
			double var8;
			if (par1Entity instanceof EntityLivingBase) {
				EntityLivingBase var10 = (EntityLivingBase)par1Entity;
				var8 = var10.posY + (double)var10.getEyeHeight() - (getPlayer().posY + (double)getPlayer().getEyeHeight());
			} else {
				var8 = (par1Entity.boundingBox.minY + par1Entity.boundingBox.maxY) / 2.0D - (getPlayer().posY + (double)getPlayer().getEyeHeight());
			}
			double var14 = (double)MathHelper.sqrt_double(var4 * var4 + var6 * var6);
			float var12 = (float)(Math.atan2(var6, var4) * 180.0D / Math.PI) - 90.0F;
			float var13 = (float)(-(Math.atan2(var8, var14) * 180.0D / Math.PI));
			float pitch = updateRotation(aimPitch, var13, 180F);
			float yaw = updateRotation(aimYaw, var12, 180F);
			if (aimPitch < pitch - 10) {
				aimPitch += lookSpeed;
				if (aimPitch > pitch + 10)
					aimPitch = pitch;
			}
			if (aimPitch > pitch + 10) {
				aimPitch -= lookSpeed;
				if (aimPitch < pitch - 10)
					aimPitch = pitch;
			}
			if (aimYaw < yaw - 10) {
				aimYaw += lookSpeed;
				if (aimYaw > yaw + 10)
					aimYaw = yaw;
			}
			if (aimYaw > yaw + 10) {
				aimYaw -= lookSpeed;
				if (aimYaw < yaw - 10)
					aimYaw = yaw;
			}
		}
		
		public void lookBack() {
			if (aimPitch == null)
				aimPitch = getPlayer().rotationPitch;
			if (aimYaw == null)
				aimYaw = getPlayer().rotationYaw;
			float pitch = getPlayer().rotationPitch;
			float yaw = getPlayer().rotationYaw;
			if (aimPitch < pitch - 10) {
				aimPitch += lookSpeed;
				if (aimPitch > pitch + 10)
					aimPitch = pitch;
			}
			if (aimPitch > pitch + 10) {
				aimPitch -= lookSpeed;
				if (aimPitch < pitch - 10)
					aimPitch = pitch;
			}
			if (aimYaw < yaw - 10) {
				aimYaw += lookSpeed;
				if (aimYaw > yaw + 10)
					aimYaw = yaw;
			}
			if (aimYaw > yaw + 10) {
				aimYaw -= lookSpeed;
				if (aimYaw < yaw - 10)
					aimYaw = yaw;
			}
		}
		
		public EntityClientPlayerMP getPlayer() {
			return getUtils().getPlayer();
		}
		
		public float updateRotation(float par1, float par2, float par3) {
			float var4 = MathHelper.wrapAngleTo180_float(par2 - par1);
			if (var4 > par3)
				var4 = par3;
			if (var4 < -par3)
				var4 = -par3;
			return par1 + var4;
		}
		
	}
	
}
