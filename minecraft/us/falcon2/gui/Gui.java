package us.falcon2.gui;

import net.minecraft.client.renderer.OpenGlHelper;
import net.minecraft.client.renderer.Tessellator;
import org.lwjgl.opengl.GL11;

public class Gui {

	public static void drawRectBorder(float x, float y, float x2, float y2, int fillColor, int borderColor, float borderSize) {
		drawRect(x, y, x + borderSize, y2, borderColor);
		drawRect(x2 - borderSize, y, x2, y2, borderColor);
		drawRect(x + borderSize, y, x2 - borderSize, y + borderSize, borderColor);
		drawRect(x + borderSize, y2 - borderSize, x2 - borderSize, y2, borderColor);
		drawRect(x + borderSize, y + borderSize, x2 - borderSize, y2 - borderSize, fillColor);
	}
	
	public static void drawGradientRectBorder(float x, float y, float x2, float y2, int fillColorTop, int fillColorBottom, int borderColor, float borderSize) {
		drawRect(x, y, x + borderSize, y2, borderColor);
		drawRect(x2 - borderSize, y, x2, y2, borderColor);
		drawRect(x + borderSize, y, x2 - borderSize, y + borderSize, borderColor);
		drawRect(x + borderSize, y2 - borderSize, x2 - borderSize, y2, borderColor);
		drawGradientRect(x + borderSize, y + borderSize, x2 - borderSize, y2 - borderSize, fillColorTop, fillColorBottom);
	}
	
	public static void drawRect(float par0, float par1, float par2, float par3, int par4) {
		float var5;
		if (par0 < par2) {
			var5 = par0;
			par0 = par2;
			par2 = var5;
		}
		if (par1 < par3) {
			var5 = par1;
			par1 = par3;
			par3 = var5;
		}
		float var10 = (float)(par4 >> 24 & 255) / 255.0F;
		float var6 = (float)(par4 >> 16 & 255) / 255.0F;
		float var7 = (float)(par4 >> 8 & 255) / 255.0F;
		float var8 = (float)(par4 & 255) / 255.0F;
		Tessellator var9 = Tessellator.instance;
		GL11.glEnable(GL11.GL_BLEND);
		GL11.glDisable(GL11.GL_TEXTURE_2D);
		OpenGlHelper.glBlendFunc(770, 771, 1, 0);
		GL11.glColor4f(var6, var7, var8, var10);
		var9.startDrawingQuads();
		var9.addVertex((double)par0, (double)par3, 0.0D);
		var9.addVertex((double)par2, (double)par3, 0.0D);
		var9.addVertex((double)par2, (double)par1, 0.0D);
		var9.addVertex((double)par0, (double)par1, 0.0D);
		var9.draw();
		GL11.glEnable(GL11.GL_TEXTURE_2D);
		GL11.glDisable(GL11.GL_BLEND);
	}

	 public static void drawGradientRect(float par1, float par2, float par3, float par4, int par5, int par6) {
		 float zLevel = 0.0F;
		 float var7 = (float)(par5 >> 24 & 255) / 255.0F;
		 float var8 = (float)(par5 >> 16 & 255) / 255.0F;
		 float var9 = (float)(par5 >> 8 & 255) / 255.0F;
		 float var10 = (float)(par5 & 255) / 255.0F;
		 float var11 = (float)(par6 >> 24 & 255) / 255.0F;
		 float var12 = (float)(par6 >> 16 & 255) / 255.0F;
		 float var13 = (float)(par6 >> 8 & 255) / 255.0F;
		 float var14 = (float)(par6 & 255) / 255.0F;
		 GL11.glDisable(GL11.GL_TEXTURE_2D);
		 GL11.glEnable(GL11.GL_BLEND);
		 GL11.glDisable(GL11.GL_ALPHA_TEST);
		 OpenGlHelper.glBlendFunc(770, 771, 1, 0);
		 GL11.glShadeModel(GL11.GL_SMOOTH);
		 Tessellator var15 = Tessellator.instance;
		 var15.startDrawingQuads();
		 var15.setColorRGBA_F(var8, var9, var10, var7);
		 var15.addVertex((double)par3, (double)par2, (double)zLevel);
		 var15.addVertex((double)par1, (double)par2, (double)zLevel);
		 var15.setColorRGBA_F(var12, var13, var14, var11);
		 var15.addVertex((double)par1, (double)par4, (double)zLevel);
		 var15.addVertex((double)par3, (double)par4, (double)zLevel);
		 var15.draw();
		 GL11.glShadeModel(GL11.GL_FLAT);
		 GL11.glDisable(GL11.GL_BLEND);
		 GL11.glEnable(GL11.GL_ALPHA_TEST);
		 GL11.glEnable(GL11.GL_TEXTURE_2D);
	 }

}
