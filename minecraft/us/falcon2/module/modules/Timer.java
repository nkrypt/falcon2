package us.falcon2.module.modules;

import us.falcon2.Falcon2;
import us.falcon2.command.Command;
import us.falcon2.event.Event;
import us.falcon2.event.events.EventOnTick;
import us.falcon2.module.Module;

public class Timer extends Module {

	private static int countdownTime = 60;
	private static Integer currentTime = null;
	private static Long delay = 1000L, last = null;
	
	public void drawTimeString(float x, float y) {
		if (!getEnabled())
			return;
		if (currentTime == null)
			return;
		if (currentTime < 0) {
			int color = 0xffffffff;
			if (currentTime == -2 || currentTime == -4 || currentTime == -6)
				color = 0xffff0000;
			getUtils().getMinecraft().fontRenderer.drawStringWithShadow("Time Ended", (int) x, (int) y, color);
			if (currentTime <= -7)
				onDisabled();
		} else {
			getUtils().getMinecraft().fontRenderer.drawStringWithShadow(getTimeString(), (int) x, (int) y, 0xffffffff);
		}
	}
	
	@Override
	public String getName() {
		return "Timer";
	}

	private String getTimeString() {
		String timeString = "00:00:00";
		try {
			String hours = ((int) currentTime / 3600) + "";
			if (hours.length() < 2)
				hours = "0" + hours;
			String minutes = ((int) (currentTime % 3600) / 60) + "";
			if (minutes.length() < 2)
				minutes = "0" + minutes;
			String seconds = ((int) currentTime % 60) + "";
			if (seconds.length() < 2)
				seconds = "0" + seconds;
			timeString = hours + ":" + minutes + ":" + seconds;
		} catch (Exception e) {}
		return timeString;
	}
	
	@Override
	public void onCreation() {
		getCommandManager().getCommands().add(new Command() {

			@Override
			public String getCommand() {
				return ".timer";
			}

			@Override
			public void runCommand(String args) {
				String[] arguments = args.split(" ");
				if (arguments[0].equalsIgnoreCase("set")) {
					String timeVal = null;
					try {
						timeVal = arguments[1].substring(arguments[1].length() - 1).toLowerCase();
					} catch (Exception e) {}
					Integer value = Integer.parseInt(arguments[1].substring(0, arguments[1].length() - 1));
					if (value != null) {
						if (timeVal == null) {
							getUtils().addChatMessage(getUtils().getClientPrefixError() + "Invalid time type.");
						} else if (timeVal.equals("m")) {
							countdownTime = value * 60;
							getUtils().addChatMessage(getUtils().getClientPrefix() + "Timer set to '\2477" + value + "\247f' minutes.");
						} else if (timeVal.equals("s")) {
							countdownTime = value;
							getUtils().addChatMessage(getUtils().getClientPrefix() + "Timer set to '\2477" + value + "\247f' seconds.");
						}
					} else {
						getUtils().addChatMessage(getUtils().getClientPrefixError() + "Invalid time amount.");
					}
				} else if (arguments[0].equalsIgnoreCase("start")){
					onEnabled();
					getUtils().addChatMessage(getUtils().getClientPrefix() + "Timer started.");
				} else if (arguments[0].equalsIgnoreCase("stop")){
					onDisabled();
					getUtils().addChatMessage(getUtils().getClientPrefix() + "Timer stopped.");
				}
			}

			@Override
			public void showCommands() {
				getUtils().addChatMessage(getUtils().getClientPrefix() + ".timer set <\2477value\247f>[\2477m\247f/\2477s\247f] - Sets the time.");
				getUtils().addChatMessage(getUtils().getClientPrefix() + ".timer [\2477start\247f/\2477stop\247f] - Starts or stops the Timer.");
			}

		});
	}
	
	@Override
	public void onDisabled() {
		super.onDisabled();
		currentTime = null;
	}
	
	@Override
	public void onEnabled() {
		super.onEnabled();
		currentTime = null;
	}
	
	@Override
	public void onEvent(Event event) {
		if (!getEnabled())
			return;
		if (event instanceof EventOnTick) {
			if (currentTime == null)
				currentTime = countdownTime;
			if (last == null || getUtils().getSysTime() > last + delay) {
				last = getUtils().getSysTime();
				currentTime -= 1;
				if (currentTime <= 0) {
					
				}
			}
		}
	}
	
}
