package us.falcon2.gui.override;

import org.lwjgl.opengl.GL11;

import us.falcon2.Falcon2;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.renderer.OpenGlHelper;

public class GuiButtonOverride extends GuiButton {

	public GuiButtonOverride(int par1, int par2, int par3, String par6Str) {
		super(par1, par2, par3, 200, 20, par6Str);
	}
	
	public GuiButtonOverride(int par1, int par2, int par3, int par4, int par5, String par6Str) {
		super(par1, par2, par3, par4, par5, par6Str);
	}

	@Override
	public void drawButton(Minecraft mc, int i, int j) {
		if (field_146125_m) {
            FontRenderer var4 = mc.fontRenderer;
            mc.getTextureManager().bindTexture(field_146122_a);
            GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
            field_146123_n = i >= field_146128_h && j >= field_146129_i && i < field_146128_h + field_146120_f && j < field_146129_i + field_146121_g;
            int var5 = getHoverState(field_146123_n);
            GL11.glEnable(GL11.GL_BLEND);
            OpenGlHelper.glBlendFunc(770, 771, 1, 0);
            GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
            drawTexturedModalRect(field_146128_h, field_146129_i, 0, 46 + var5 * 20, field_146120_f / 2, field_146121_g);
            drawTexturedModalRect(field_146128_h + field_146120_f / 2, field_146129_i, 200 - field_146120_f / 2, 46 + var5 * 20, field_146120_f / 2, field_146121_g);
            mouseDragged(mc, i, j);
            int x = field_146128_h;
            int y = field_146129_i;
            int width = field_146120_f;
            Falcon2.getFontManager().getFont("guiButtonFont").drawCenteredString((enabled ? (field_146123_n ? "\247e":""):"\2477") + displayString, x + width / 2, y + 6, 0xffffffff, true);
		}
	}
	
}
