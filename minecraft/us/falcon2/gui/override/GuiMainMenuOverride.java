package us.falcon2.gui.override;

import us.falcon2.Falcon2;
import us.falcon2.gui.GuiAccountLogin;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiMainMenu;
import net.minecraft.client.resources.I18n;

public class GuiMainMenuOverride extends GuiMainMenu {

	@Override
	public void initGui() {
		super.initGui();
		int var3 = height / 4 + 48;
		buttonList.add(/** TODO: Falcon2 **/ new us.falcon2.gui.override.GuiButtonOverride(20, width / 2 - 100, var3 + 48, "Account Login"));
	}
	
	@Override
	public void actionPerformed(GuiButton button) {
		super.actionPerformed(button);
		switch(button.id) { 
		case 20:
			mc.displayGuiScreen(new GuiAccountLogin(this));
			break;
		}
	}
	
	@Override
	public void drawScreen(int i, int j, float k) {
		super.drawScreen(i, j, k);
		String str[] = {
				"\2477Falcon2",
				"\2477Developed by nKRYPT & jordinjm"
		};
		mc.fontRenderer.drawStringWithShadow(str[0], 2, height - 20, 0xffffffff);
		mc.fontRenderer.drawStringWithShadow(str[1], width - (mc.fontRenderer.getStringWidth(str[1]) + 2), height - 20, 0xffffffff);
	}
	
}
