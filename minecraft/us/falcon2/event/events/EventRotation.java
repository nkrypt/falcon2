package us.falcon2.event.events;

import net.minecraft.network.Packet;
import us.falcon2.event.EventPrePost;

public class EventRotation extends EventPrePost {

	private Packet packet;
	
	public Packet getPacket() {
		return packet;
	}
	
	public void setPacket(Packet packet) {
		this.packet = packet;
	}
	
}
