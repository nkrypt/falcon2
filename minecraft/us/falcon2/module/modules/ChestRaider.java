package us.falcon2.module.modules;

import java.util.ArrayList;
import net.minecraft.block.Block;
import net.minecraft.block.BlockChest;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.inventory.GuiChest;
import net.minecraft.init.Blocks;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.network.play.client.C08PacketPlayerBlockPlacement;
import us.falcon2.event.Event;
import us.falcon2.event.EventState;
import us.falcon2.event.events.EventOnUpdate;
import us.falcon2.module.Module;

public class ChestRaider extends Module {
	
	private static ArrayList<BlockChest> openedChests = new ArrayList<BlockChest>();

	@Override
	public String getName() {
		return "ChestRaider";
	}

	private void handleRaiding() {
		int radius = 3;
		for (int x = -radius; x < radius; x++) {
			for (int y = -radius; y < radius; y++) {
				for (int z = -radius; z < radius; z++) {
					int posX = (int) getUtils().getPlayer().posX + x;
					int posY = (int) getUtils().getPlayer().posY + y;
					int posZ = (int) getUtils().getPlayer().posZ + z;
					Block block = getUtils().getWorld().getBlock(posX, posY, posZ);
					if (block == Blocks.chest) {
						//getUtils().getSendQueue().addToSendQueue(new C08PacketPlayerBlockPlacement(posX, posY, posZ, 1, getUtils().getPlayer().getCurrentEquippedItem(), 0, 0, 0));
						BlockChest chest = (BlockChest) block;
						IInventory chestInventory = chest.func_149951_m(getUtils().getWorld(), posX, posY, posZ);
						GuiChest guiChest = new GuiChest(getUtils().getPlayer().inventory, chestInventory);
						getUtils().getMinecraft().displayGuiScreen(guiChest);
						startRaid(chestInventory, guiChest.field_147002_h.windowId);
						openedChests.add(chest);
					}
				}
			}
		}
	}

	@Override
	public void onCreation() {
		setKeybind("V");
	}

	@Override
	public void onEvent(Event event) {
		if (!getEnabled())
			return;
		if (event instanceof EventOnUpdate) {
			EventOnUpdate eventOnUpdate = (EventOnUpdate) event;
			if (eventOnUpdate.getState() == EventState.PRE)
				handleRaiding();
		}
	}
	
	private void startRaid(IInventory inventory, int windowID) {
    	ArrayList<Integer> items = new ArrayList<Integer>();
    	for (int slot = 0; slot < inventory.getSizeInventory(); slot++) {
    		try {
        		if (inventory.getStackInSlot(slot) != null)
        			items.add(slot);
    		} catch (Exception e) { e.printStackTrace(); }
    	}
    	if (!items.isEmpty()) {
    		for (int slot = 9; slot <= 44; slot++) {
    			try {
    				if (getUtils().getPlayer().inventory.getStackInSlot(slot) == null) {
    					int chestSlot = getFirstSlot(items);
    					items.remove(chestSlot);
    					getUtils().getController().windowClick(windowID, chestSlot, 0, 0, getUtils().getPlayer());
    					getUtils().getController().windowClick(0, slot, 0, 0, getUtils().getPlayer());
    				}
    			} catch (Exception e) {}
    		}
    	}
    	getUtils().getMinecraft().displayGuiScreen(null);
    }
	
	private Integer getFirstSlot(ArrayList<Integer> array) {
		for (Integer item : array)
			return item;
		return 0;
	}

}
