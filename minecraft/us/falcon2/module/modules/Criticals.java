package us.falcon2.module.modules;

import net.minecraft.network.play.client.C03PacketPlayer;
import us.falcon2.event.Event;
import us.falcon2.event.EventState;
import us.falcon2.event.events.EventMotionUpdate;
import us.falcon2.module.Module;

public class Criticals extends Module {

	private static boolean jumped = false;
	private static float fallDist = 0.0F;
	
	@Override
	public String getName() {
		return "Criticals";
	}
	
	@Override
	public void onEvent(Event event) {
		if (!getEnabled())
			return;
		if (getUtils().getPlayer().capabilities.isFlying) {
			fallDist = 0;
			return;
		}
		if (getUtils().getGameSettings().keyBindJump.isPressed())
			return;
		if (event instanceof EventMotionUpdate) {
			EventMotionUpdate eventMotionUpdate = (EventMotionUpdate) event;
			if (getUtils().getModuleByClass(Aura.class).getEnabled() && Aura.target != null) {
				if (jumped) {
					if (getUtils().getPlayer().onGround)
						jumped = false;
					else
						return;
				}
				if (eventMotionUpdate.getState() == EventState.PRE) {
					if (getUtils().getPlayer().onGround) {
						getUtils().getPlayer().onGround = false;
						getUtils().getPlayer().motionY = 0.2D;
						fallDist += 0.125F;
					}
				} else {
					getUtils().getPlayer().onGround = true;
					getUtils().getPlayer().motionY = -0.3D;
					if (fallDist >= 3.0F) {
						fallDist = 0.0F;
						getUtils().getSendQueue().addToSendQueue(new C03PacketPlayer.C04PacketPlayerPosition(getUtils().getPlayer().posX, getUtils().getPlayer().boundingBox.minY + 1D, getUtils().getPlayer().posY + 1D, getUtils().getPlayer().posZ, getUtils().getPlayer().onGround));
						jumped = true;
					}
				}
			}
		}
	}
	
}
