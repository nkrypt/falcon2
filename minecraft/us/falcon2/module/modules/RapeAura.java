package us.falcon2.module.modules;

import us.falcon2.Falcon2;
import us.falcon2.event.*;
import us.falcon2.event.events.*;

public class RapeAura extends Aura {
	
	@Override
	public String getName() {
		return "RapeAura";
	}
	
	@Override
	protected void handleAura() {
		setTargetEntity();
		if (target != null) {
			for (int i = 0; i < 2; i++) {
				EventAuraAttack eventAuraAttack = new EventAuraAttack();
				eventAuraAttack.setState(EventState.PRE);
				Falcon2.getEventManager().callEvent(eventAuraAttack);
				getUtils().getPlayer().inventory.currentItem = 0;
				getUtils().getPlayer().swingItem();
				getUtils().getController().attackEntity(getUtils().getPlayer(), target);
				eventAuraAttack.setState(EventState.POST);
				Falcon2.getEventManager().callEvent(eventAuraAttack);
			}
		}
	}
	
	@Override
	public void onCreation() {
		setKeybind("C");
	}
	
	@Override
	public void onEvent(Event event) {
		if (!getEnabled())
			return;
		if (event instanceof EventMotionUpdate) {
			EventMotionUpdate eventOnUpdate = (EventMotionUpdate) event;
			if (eventOnUpdate.getState() == EventState.PRE)
				handleAura();
		}
	}
	
}
