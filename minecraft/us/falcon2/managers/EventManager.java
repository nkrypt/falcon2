package us.falcon2.managers;

import us.falcon2.Falcon2;
import us.falcon2.event.Event;
import us.falcon2.module.Module;

public class EventManager {

	public void callEvent(Event event) {
		for (Module module : Falcon2.getModuleManager().getModules()) {
			try {
				module.onEvent(event);
			} catch (Exception e) {}
		}
	}
	
}
