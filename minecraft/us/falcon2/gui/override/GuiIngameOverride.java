package us.falcon2.gui.override;

import us.falcon2.Falcon2;
import us.falcon2.module.Module;
import us.falcon2.module.modules.*;
import me.isuzutsuki.betterfonts.betterfonts.StringCache;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.GuiIngame;

public class GuiIngameOverride extends GuiIngame {

	private final Minecraft mc;
	
	public GuiIngameOverride(Minecraft mc) {
		super(mc);
		this.mc = mc;
	}
	
	@Override
	public void renderGameOverlay(float par1, boolean par2, int par3, int par4) {
		super.renderGameOverlay(par1, par2, par3, par4);
		if (mc.gameSettings.showDebugInfo)
			return;
		FontRenderer font = mc.fontRenderer;
		int y = 2;
		font.drawStringWithShadow("\2477Falcon\247c2", 2, y, 0xffffffff); y += 9;
		((Timer) Falcon2.getModuleManager().getModuleByClass(Timer.class)).drawTimeString(2, y);
		y = 2;
		int screenWidth = (int) ((float) mc.displayWidth / 2);
		((Array) Falcon2.getModuleManager().getModuleByClass(Array.class)).drawArray(screenWidth, y);
	}
	
}