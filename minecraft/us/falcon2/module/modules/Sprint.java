package us.falcon2.module.modules;

import us.falcon2.Falcon2;
import us.falcon2.command.Command;
import us.falcon2.event.Event;
import us.falcon2.event.EventState;
import us.falcon2.event.events.*;
import us.falcon2.module.Module;

public class Sprint extends Module {
	
	public static double speed = 1.0D;
	
	@Override
	public String getName() {
		return "Sprint";
	}
	
	@Override
	public void onCreation() {
		setKeybind("N");
		getCommandManager().getCommands().add(new Command() {
			
			@Override
			public String getCommand() {
				return ".ss";
			}
			
			@Override
			public void runCommand(String args) {
				double value = Double.parseDouble(args);
				speed = value;
				getUtils().addChatMessage(getUtils().getClientPrefix() + "Sprint speed set to '\2477" + value + "\247f'.");
			}
			
			@Override
			public void showCommands() {
				getUtils().addChatMessage(getUtils().getClientPrefix() + ".ss <\2477value\247f> - Sets the Sprint's speed.");
			}
			
		});
	}
	
	@Override
	public void onEvent(Event event) {
		if (!getEnabled())
			return;
		if (getUtils().getPlayer().movementInput.moveForward <= 0.0F)
			return;
		if (event instanceof EventOnUpdate) {
			EventOnUpdate eventOnUpdate = (EventOnUpdate) event;
			if (eventOnUpdate.getState() == EventState.PRE)
				getUtils().getPlayer().setSprinting(true);
		} else if (event instanceof EventMoveEntity) {
			EventMoveEntity eventMoveEntity = (EventMoveEntity) event;
			eventMoveEntity.setX(eventMoveEntity.getX() * speed);
			eventMoveEntity.setZ(eventMoveEntity.getZ() * speed);
		}
	}
	
}
